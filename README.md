# Open Source Retrospective Template

Template for running retrospectives within an open source community. 

**Goal:** To solicit feedback and encourage an open conversation among community members and project maintainers or company employees.

## Working Agreements

* Try to minimize distractions
* Set yourself to Do-Not-Disturb
* Quit Slack, Twitter, etc.
* Don’t interrupt others
* When criticizing/saying something negative, focus on actions not people
* Don’t assign blame
* Be careful about speculating

If discussion of a single card/subject goes over five minutes and becomes a complex discussion/debate, it needs to be an action item to schedule a separate, focused meeting about the issue. It is likely that a simple majority of action items, at least, will be in the vein of “schedule meeting to talk about $thing” and that is fine. 

## Voting
Everyone gets two votes per column (so two votes for good, two votes for not so good), plus an additional vote that can go in either column (so five altogether). It’s ok to abstain from voting, but we encourage people to think this through before committing to doing that. We’ll cover the top 3 items from each category.

## Roles
### Facilitator: 
**Before retro:** archives old good/bad cards. 

**During retro:** 
* Seeks feedback from everyone (including by offering quiet folks the chance to talk). 
* “Can-of-worms”es too-involved discussions. 
* Ensures at least the top three cards in each column have adequate time for discussion. 
* Keeps people aware of time. 
* Groups cards if needed, and guides discussion based on votes. 
* Keeps people to the working agreements. 
* Keeps things moving, productive, and civil.

### Recorder: 
* Writes down action items as they arise in discussion. 
* Clarifies that the group agrees action is needed and what the group thinks the action should be. 
* Screenshots the board at the end and adds it to the notes doc for that retro. 

### Timeline:
* 0:00-5:00: review previous action items if necessary; add last-minute cards. (5 minutes)
* 5:00-8:00: read What's Going Well (3 minutes)
* 8:00-10:00: vote on What's Going Well (2 minutes) - remind folks they get two votes, and they also have a wildcard that they can put in either column
* 11:00-25:00: discuss top What's Going Well and create action items, if any (14 minutes)
* 25:00-28:00: read What's Not Going Well (3 minutes) - remind folks they get two votes, and if they haven’t used their wildcard yet, they have a third available
* 28:00-30:00: vote on What's Not Going Well (2 minutes)
* 31:00-50:00: discuss top What's Not Going Well and create action items, if any (19 minutes)
* 50:00-end: finalize and assign action items, meta-feedback (10ish minutes)

It’s ok if things move a little faster than this, but let a good long moment of weird silence elapse before deciding to skip forward. Leave at least five minutes at the end to parse and assign action items.

---

_Big thanks and credit to [Breanne Boland](https://twitter.com/breanneboland) for creating the foundation for this working agreement framework for online retrospectives while working at [Nylas](https://www.nylas.com/)._